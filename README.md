## Software and Data

- [WiFiMapping package](https://gitlab.vgiscience.de/topikos/wifimapping/tree/master), a python package for creating topological (indoor) maps from WiFi and motion data. The data can, for example, be collected through smartphones. Various data sources are supported (.csv-files, [EveryAware](http://cs.everyaware.eu), MongoDB database).
- WiFi Distances for Topological Indoor Mapping evaluation result dataset ([Download](https://kde.cs.uni-kassel.de/topikos/wifi_distances_evaluation.zip)). The dataset contains performance metrics calculated for various measures of distance and probability estimation techniques for different smartphones.


## Original Proposal

We study the emergence of topological, topical, and social maps of short-lived indoor events such as conventions, conferences and trade fairs, in a crowd-sourced manner with little to no effort by participants of the event (and possibly no support by the event organizers). Following the notion 'common sense geography' as coined by the DFG Excellence Cluster 264 'Topoi', we will consider scenarios where no explicit infrastructure for localisation and navigation is provided, where no specific mapping activities are performed prior to the event, and where participants are not required to deviate from their usual behavior and to wear any systems beside their smartphones.

Due to these constraints, we assume that the construction of a topographical map (i. e., a floor plan reflecting distance) will not be possible. Therefore, our focus is on topological maps, which represent in a structure-preserving form (indoor) 'places', i. e., locations where groups of people come together, and 'paths' connecting them. We will concretise these definitions for places and paths, and will develop algorithms for detecting them based on the fingerprints of WiFi signal strengths, which the participants constantly measure.

Topics can be extracted from the textual resources of the participants (e. g., their homepage and their tweets) using text mining. Topical maps augment topological maps by semantic information about the topics that participants who are visiting a place 'will bring along'. Social maps additionally provide information about who knows whom and by information about who talked when and where to whom.

For all three types of maps, we will also develop suitable map drawing algorithms. These maps may be used by participants for obtaining an overview over the event, and for identifying and finding friends or like-minded people. For people and thematic places, we will develop a personalised recommender, and navigational support to ease the search for the recommended person or site. Another use case for these maps are analyses, e. g., for studying the usage of space during an event by individuals and groups.

Our algorithms will be evaluated both on existing, historical data and with volunteers using a prototype implementation, which will be based on the social conference management platform Conferator and the Sensor Data Collection Framework, both developed in our group.
